# I Did

> Tracks daily task performance.

## Overview

This may be the most [MeWare](https://ericsink.com/articles/Yours_Mine_Ours.html)  software you'll find. It tracks repeating (daily) tasks and reports on how well you're doing them. It's not for everyone, but it works for me.

## Installation

### Prerequisites

* Rust

### Steps

* Clone the repository
* `cd` to the directory
* `cargo install --path .`

## Usage

### Add a Task

```console
$ idid add <TASK NAME>
```

### Delete a Task

```console
$ idid delete <TASK ID>
```

### Record Completion of a Task

```console
$ idid do <TASK ID> [DATE]
```

### Displays Info

```console
$ idid info
```

### List Tasks

```console
$ idid list [TASK ID]
```

### Rename a Task

```console
$ idid rename <TASK ID> <NEW TASK NAME>
```

### Report on a Task

```console
$ idid report [TASK ID] [YEAR]
```

## Examples

```console
$ idid add "Read for 30 minutes"
added task 'Read for 30 minutes' with id 1

$ idid do 1
you did task with id 1 on 2024-01-09

$ idid report
1: Read for 30 minutes
  2024-01-09
```

## License

Copyright &copy; 2024 Rob Warner

Licensed under the [MIT License](https://hoop33.mit-license.org/)
