use clap::{Parser, Subcommand};
use idid::Tasks;

/// Track daily task performance.
#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    command: Commands,
    /// The path to the task database.
    #[arg(short, long)]
    path: Option<String>,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Adds a new task.
    #[command(arg_required_else_help = true)]
    Add {
        /// The task to add.
        task: String,
    },
    /// Deletes a task.
    #[command(arg_required_else_help = true)]
    Delete {
        /// The task to delete.
        id: String,
    },
    /// Records completion of a task.
    #[command(arg_required_else_help = true)]
    Do {
        /// The task.
        id: String,
        /// The date (YYYY-MM-DD) (Default: today).
        date: Option<String>,
    },
    /// Displays information about idid.
    #[command(arg_required_else_help = false)]
    Info {},
    /// Lists all tasks.
    List {
        /// The task to list.
        id: Option<String>,
    },
    /// Renames a task.
    #[command(arg_required_else_help = true)]
    Rename {
        /// The task to rename.
        id: String,
        /// The new name of the task.
        name: String,
    },
    /// Reports on tasks.
    #[command(arg_required_else_help = false)]
    Report {
        /// The task to report on.
        id: Option<String>,
        /// The year to report on.
        year: Option<String>,
    },
}

fn main() {
    let args = Args::parse();

    match Tasks::new(args.path) {
        Ok(tasks) => {
            let result = match args.command {
                Commands::Add { task } => tasks.add(task),
                Commands::Delete { id } => tasks.delete(id),
                Commands::Do { id, date } => tasks.do_task(id, date),
                Commands::Info {} => tasks.info(),
                Commands::List { id } => tasks.list(id),
                Commands::Rename { id, name } => tasks.rename(id, name),
                Commands::Report { id, year } => tasks.report(id, year),
            };

            match result {
                Ok(result) => println!("{}", result),
                Err(e) => eprintln!("Error: {}", e),
            }
        }
        Err(e) => eprintln!("Error: {}", e),
    }
}
