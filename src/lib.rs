use anyhow::{anyhow, Result};
use chrono::{Local, NaiveDate};
use rusqlite::{params, Connection, Rows};
use std::collections::BTreeMap;

#[derive(Debug, PartialEq)]
struct Task {
    id: u32,
    name: String,
    completions: Option<Vec<Completion>>,
}

impl Task {
    fn new(id: u32, name: String) -> Self {
        Self {
            id,
            name,
            completions: None,
        }
    }

    fn from_row(row: &rusqlite::Row) -> rusqlite::Result<Self> {
        let id = row.get(0)?;
        let name = row.get(1)?;
        Ok(Self::new(id, name))
    }

    fn add_completion(&mut self, completion: Completion) {
        match self.completions {
            Some(ref mut completions) => completions.push(completion),
            None => self.completions = Some(vec![completion]),
        }
    }

    fn to_string(&self) -> String {
        match self.completions {
            Some(ref completions) => {
                let mut result = format!("{}: {} ({})", self.id, self.name, completions.len());
                for report in completions {
                    result.push_str(&format!("\n  {}", report.date));
                }
                result.push_str(&format!("\nTotal: {}", completions.len()));
                result
            }
            None => format!("{}: {}", self.id, self.name),
        }
    }
}

#[derive(Debug, PartialEq)]
struct Completion {
    id: u32,
    task_id: u32,
    date: NaiveDate,
}

impl Completion {
    fn from_row(row: &rusqlite::Row) -> rusqlite::Result<Self> {
        Ok(Completion {
            id: row.get(0)?,
            task_id: row.get(1)?,
            date: row.get(2)?,
        })
    }
}

#[derive(Debug)]
pub struct Tasks {
    conn: Connection,
}

impl Tasks {
    pub fn new(path: Option<String>) -> Result<Self> {
        let path = path.unwrap_or_else(|| {
            xdg::BaseDirectories::with_prefix("idid")
                .map(|xdg_dirs| {
                    xdg_dirs
                        .place_data_file("idid.db")
                        .expect("failed to create data file")
                        .to_str()
                        .expect("failed to convert data file path to string")
                        .to_string()
                })
                .expect("failed to get data file path")
        });
        let conn = Connection::open(path)?;

        conn.execute(
            "CREATE TABLE IF NOT EXISTS tasks (
                id INTEGER PRIMARY KEY,
                name TEXT NOT NULL UNIQUE
            )",
            (),
        )?;

        conn.execute(
            "CREATE TABLE IF NOT EXISTS completions (
                id INTEGER PRIMARY KEY,
                task_id INTEGER NOT NULL,
                date TEXT NOT NULL,
                FOREIGN KEY (task_id) REFERENCES tasks (id)
            )",
            (),
        )?;

        Ok(Self { conn })
    }

    pub fn add(&self, task: String) -> Result<String> {
        self.conn
            .execute("INSERT INTO tasks (name) VALUES (?)", [&task])?;
        Ok(format!(
            "added task '{}' with id {}",
            task,
            self.conn.last_insert_rowid()
        ))
    }

    pub fn delete(&self, id: String) -> Result<String> {
        let id = id.parse::<u32>()?;
        self.conn.execute("DELETE FROM tasks WHERE id = ?", [&id])?;
        match self.conn.changes() {
            0 => Err(anyhow!("No task with id {}", id)),
            _ => Ok(format!("deleted task with id {}", id)),
        }
    }

    pub fn do_task(&self, id: String, date: Option<String>) -> Result<String> {
        let id = id.parse::<u32>()?;
        let date = match date {
            Some(date) => NaiveDate::parse_from_str(&date, "%Y-%m-%d")?,
            None => Local::now().naive_local().date(),
        }
        .format("%Y-%m-%d")
        .to_string();

        match self.already_did_task_on_date(id, date.clone()) {
            Ok(true) => Err(anyhow!("You already did task with id {} on {}", id, date)),
            Ok(false) => {
                self.conn.execute(
                    "INSERT INTO completions (task_id, date) VALUES (?, ?)",
                    params![&id, &date],
                )?;
                match self.conn.changes() {
                    0 => Err(anyhow!("No task with id {}", id)),
                    _ => Ok(format!("you did task with id {} on {}", id, date)),
                }
            }
            Err(e) => Err(anyhow!("Error: {}", e)),
        }
    }

    pub fn info(&self) -> Result<String> {
        self.conn.path().map_or_else(
            || Err(anyhow!("No database path")),
            |path| Ok(format!("Database path: {}", path)),
        )
    }

    pub fn list(&self, id: Option<String>) -> Result<String> {
        let tasks = match id {
            Some(id) => vec![self.list_one(id)?],
            None => self.list_all()?,
        };

        Ok(tasks
            .into_iter()
            .map(|task| task.to_string())
            .collect::<Vec<_>>()
            .join("\n"))
    }

    pub fn rename(&self, id: String, name: String) -> Result<String> {
        let id = id.parse::<u32>()?;
        self.conn.execute(
            "UPDATE tasks SET name = ?1 WHERE id = ?2",
            params![&name, &id],
        )?;
        match self.conn.changes() {
            0 => Err(anyhow!("No task with id {}", id)),
            _ => Ok(format!("renamed task with id {} to '{}'", id, name)),
        }
    }

    pub fn report(&self, id: Option<String>, year: Option<String>) -> Result<String> {
        let tasks = match (id, year) {
            (Some(id), Some(year)) => self.tasks_with_id_and_year(id, year)?,
            (Some(id), None) => self.tasks_with_id(id)?,
            (None, Some(year)) => self.tasks_with_year(year)?,
            (None, None) => self.tasks_all()?,
        };

        Ok(tasks
            .into_iter()
            .map(|task| task.to_string())
            .collect::<Vec<_>>()
            .join("\n"))
    }

    fn list_one(&self, id: String) -> Result<Task> {
        let id = id.parse::<u32>()?;
        let mut stmt = self
            .conn
            .prepare("SELECT id, name FROM tasks WHERE id = ?1")?;
        let task = stmt.query_map([&id], Task::from_row)?.next();
        match task {
            Some(Ok(task)) => Ok(task),
            Some(Err(e)) => Err(anyhow!("Error: {}", e)),
            None => Err(anyhow!("No task with id {}", id)),
        }
    }

    fn list_all(&self) -> Result<Vec<Task>> {
        let mut stmt = self.conn.prepare("SELECT id, name FROM tasks")?;
        let rows = stmt.query_map([], Task::from_row)?;
        match rows.collect() {
            Ok(tasks) => Ok(tasks),
            Err(e) => Err(anyhow!("Error: {}", e)),
        }
    }

    fn tasks_with_id_and_year(&self, id: String, year: String) -> Result<Vec<Task>> {
        let id = id.parse::<u32>()?;
        let mut stmt = self.conn.prepare(
            "SELECT t.id, t.name, r.id, r.date
            FROM tasks t
            INNER JOIN completions r
            ON r.task_id = t.id
            WHERE t.id = ?1 AND r.date LIKE ?2
            ORDER BY t.id, r.date",
        )?;
        let rows = stmt.query(params!(&id, &format!("{}-%", year)))?;
        self.rows_to_tasks(rows)
    }

    fn tasks_with_id(&self, id: String) -> Result<Vec<Task>> {
        let id = id.parse::<u32>()?;
        let mut stmt = self.conn.prepare(
            "SELECT t.id, t.name, r.id, r.date
            FROM tasks t
            INNER JOIN completions r
            ON r.task_id = t.id
            WHERE t.id = ?1
            ORDER BY t.id, r.date",
        )?;
        let rows = stmt.query([&id])?;
        self.rows_to_tasks(rows)
    }

    fn tasks_with_year(&self, year: String) -> Result<Vec<Task>> {
        let mut stmt = self.conn.prepare(
            "SELECT t.id, t.name, r.id, r.date
            FROM tasks t
            INNER JOIN completions r
            ON r.task_id = t.id
            WHERE r.date LIKE ?1
            ORDER BY t.id, r.date",
        )?;
        let rows = stmt.query([&format!("{}-%", year)])?;
        self.rows_to_tasks(rows)
    }

    fn tasks_all(&self) -> Result<Vec<Task>> {
        let mut stmt = self.conn.prepare(
            "SELECT t.id, t.name, r.id, r.date
            FROM tasks t
            INNER JOIN completions r
            ON r.task_id = t.id
            ORDER BY t.id, r.date",
        )?;

        let rows = stmt.query([])?;
        self.rows_to_tasks(rows)
    }

    fn rows_to_tasks(&self, mut rows: Rows) -> Result<Vec<Task>> {
        let mut tasks: BTreeMap<u32, Task> = BTreeMap::new();
        while let Some(row) = rows.next()? {
            let task_id = row.get(0)?;
            let task = tasks
                .entry(task_id)
                .or_insert_with(|| Task::from_row(row).expect("failed to create task"));
            let completion = Completion {
                id: row.get(2)?,
                task_id,
                date: row.get(3)?,
            };
            task.add_completion(completion);
        }

        Ok(tasks.into_values().collect())
    }

    fn already_did_task_on_date(&self, id: u32, date: String) -> Result<bool> {
        let mut stmt = self.conn.prepare(
            "SELECT id, task_id, date
            FROM completions
            WHERE task_id = ?1 AND date = ?2",
        )?;
        let completion = stmt
            .query_map(params!(&id, &date), Completion::from_row)?
            .next();
        match completion {
            Some(Ok(_)) => Ok(true),
            Some(Err(e)) => Err(anyhow!("Error: {}", e)),
            None => Ok(false),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const DB_PATH: &'static str = "test.db";

    fn before_each() {
        clear_test_db();
    }

    fn after_each() {
        clear_test_db();
    }

    fn clear_test_db() {
        let conn = Connection::open(DB_PATH).unwrap();
        let _ = conn.execute("DELETE FROM completions", []);
        let _ = conn.execute("DELETE FROM tasks", []);
        conn.close().unwrap();
    }

    #[test]
    fn test_add() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        assert_eq!(
            tasks.add("test".to_string()).unwrap(),
            "added task 'test' with id 1"
        );

        let tasks: Vec<Task> = Connection::open(DB_PATH)
            .unwrap()
            .prepare("SELECT id, name FROM tasks")
            .unwrap()
            .query_map([], Task::from_row)
            .unwrap()
            .map(|row| row.unwrap())
            .collect();

        assert_eq!(tasks, vec![Task::new(1, "test".to_string())]);

        after_each();
    }

    #[test]
    fn test_add_should_fail_when_task_name_is_not_unique() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.add("test".to_string());
        assert_eq!(
            result.unwrap_err().to_string(),
            "UNIQUE constraint failed: tasks.name"
        );

        after_each();
    }

    #[test]
    fn test_delete_should_delete_when_task_exists() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.delete("1".to_string());
        assert_eq!(result.unwrap(), "deleted task with id 1");

        let conn = Connection::open(DB_PATH).unwrap();
        let mut stmt = conn.prepare("SELECT id, name FROM tasks").unwrap();
        let rows = stmt.query_map([], |row| Task::from_row(row)).unwrap();
        let tasks: Vec<Task> = rows.map(|row| row.unwrap()).collect();
        assert_eq!(tasks.len(), 0);

        after_each();
    }

    #[test]
    fn test_delete_should_not_delete_when_task_does_not_exist() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();

        let result = tasks.delete("1".to_string());
        assert_eq!(result.unwrap_err().to_string(), "No task with id 1");

        after_each();
    }

    #[test]
    fn test_list() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.list(None);
        assert_eq!(result.unwrap(), "1: test");

        after_each();
    }

    #[test]
    fn test_list_one() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.list(Some("1".to_string()));
        assert_eq!(result.unwrap(), "1: test");

        after_each();
    }

    #[test]
    fn test_list_all() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");
        tasks.add("test2".to_string()).expect("failed to add task2");

        let result = tasks.list(None);
        assert_eq!(result.unwrap(), "1: test\n2: test2");

        after_each();
    }

    #[test]
    fn test_rename() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.rename("1".to_string(), "test2".to_string());
        assert_eq!(result.unwrap(), "renamed task with id 1 to 'test2'");

        let conn = Connection::open(DB_PATH).unwrap();
        let mut stmt = conn.prepare("SELECT id, name FROM tasks").unwrap();
        let rows = stmt.query_map([], |row| Task::from_row(row)).unwrap();
        let tasks: Vec<Task> = rows.map(|row| row.unwrap()).collect();
        assert_eq!(tasks.len(), 1);
        assert_eq!(tasks[0].id, 1);
        assert_eq!(tasks[0].name, "test2");

        after_each();
    }

    #[test]
    fn test_rename_should_fail_when_task_does_not_exist() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();

        let result = tasks.rename("1".to_string(), "test2".to_string());
        assert_eq!(result.unwrap_err().to_string(), "No task with id 1");

        after_each();
    }

    #[test]
    fn test_already_did_task_on_date_should_return_true_when_task_was_already_done_on_date() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");
        tasks
            .do_task("1".to_string(), Some("2020-01-01".to_string()))
            .expect("failed to do task");

        let result = tasks.already_did_task_on_date(1, "2020-01-01".to_string());
        assert_eq!(result.unwrap(), true);

        after_each();
    }

    #[test]
    fn test_already_did_task_on_date_should_return_false_when_task_does_not_exist() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();

        let result = tasks.already_did_task_on_date(1, "2020-01-02".to_string());
        assert_eq!(result.unwrap(), false);

        after_each();
    }

    #[test]
    fn test_already_did_task_on_date_should_return_false_when_task_was_never_done() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.already_did_task_on_date(1, "2020-01-02".to_string());
        assert_eq!(result.unwrap(), false);

        after_each();
    }

    #[test]
    fn test_already_did_task_on_date_should_return_false_when_task_was_done_on_different_date() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");
        tasks
            .do_task("1".to_string(), Some("2020-01-01".to_string()))
            .expect("failed to do task");

        let result = tasks.already_did_task_on_date(1, "2020-01-02".to_string());
        assert_eq!(result.unwrap(), false);

        after_each();
    }

    #[test]
    fn test_do_task_should_complete_task_when_not_already_done() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");

        let result = tasks.do_task("1".to_string(), Some("2020-01-01".to_string()));
        assert_eq!(result.unwrap(), "you did task with id 1 on 2020-01-01");

        let conn = Connection::open(DB_PATH).unwrap();
        let mut stmt = conn
            .prepare("SELECT id, task_id, date FROM completions")
            .unwrap();
        let rows = stmt.query_map([], |row| Completion::from_row(row)).unwrap();
        let completions: Vec<Completion> = rows.map(|row| row.unwrap()).collect();
        assert_eq!(completions.len(), 1);
        assert_eq!(completions[0].id, 1);
        assert_eq!(completions[0].task_id, 1);
        assert_eq!(
            completions[0].date,
            NaiveDate::from_ymd_opt(2020, 1, 1).unwrap()
        );

        after_each();
    }

    #[test]
    fn test_do_task_should_return_error_when_task_was_already_done_for_date() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        tasks.add("test".to_string()).expect("failed to add task");
        tasks
            .do_task("1".to_string(), Some("2020-01-01".to_string()))
            .expect("failed to do task");

        let result = tasks.do_task("1".to_string(), Some("2020-01-01".to_string()));
        assert_eq!(
            result.unwrap_err().to_string(),
            "You already did task with id 1 on 2020-01-01"
        );

        after_each();
    }

    #[test]
    fn test_info_should_return_database_path() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();

        let result = tasks.info();

        let conn = Connection::open(DB_PATH).unwrap();
        assert_eq!(
            result.unwrap(),
            format!("Database path: {}", conn.path().unwrap())
        );

        after_each();
    }

    #[test]
    fn test_report_with_id_and_year_should_return_report_for_id_and_year() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        seed_tasks(&tasks);
        let result = tasks.report(Some("1".to_string()), Some("2023".to_string()));
        assert_eq!(
            result.unwrap(),
            "1: test (2)\n  2023-01-01\n  2023-01-02\nTotal: 2"
        );

        after_each();
    }

    #[test]
    fn test_report_with_id_should_return_report_for_id() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        seed_tasks(&tasks);
        let result = tasks.report(Some("1".to_string()), None);
        assert_eq!(
            result.unwrap(),
            "1: test (4)\n  2023-01-01\n  2023-01-02\n  2024-01-01\n  2024-01-02\nTotal: 4"
        );

        after_each();
    }

    #[test]
    fn test_report_with_year_should_return_report_for_year() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        seed_tasks(&tasks);
        let result = tasks.report(None, Some("2023".to_string()));
        assert_eq!(
            result.unwrap(),
            "1: test (2)\n  2023-01-01\n  2023-01-02\nTotal: 2\n2: test 2 (2)\n  2023-01-01\n  2023-01-02\nTotal: 2"
        );

        after_each();
    }

    #[test]
    fn test_report_should_return_report_for_all_tasks() {
        before_each();

        let tasks = Tasks::new(Some(DB_PATH.to_string())).unwrap();
        seed_tasks(&tasks);
        let result = tasks.report(None, None);
        assert_eq!(result.unwrap(), "1: test (4)\n  2023-01-01\n  2023-01-02\n  2024-01-01\n  2024-01-02\nTotal: 4\n2: test 2 (4)\n  2023-01-01\n  2023-01-02\n  2024-01-01\n  2024-01-02\nTotal: 4");

        after_each();
    }

    fn seed_tasks(tasks: &Tasks) {
        tasks.add("test".to_string()).expect("failed to add task");
        tasks
            .do_task("1".to_string(), Some("2023-01-01".to_string()))
            .expect("failed to do task");
        tasks
            .do_task("1".to_string(), Some("2023-01-02".to_string()))
            .expect("failed to do task");
        tasks
            .do_task("1".to_string(), Some("2024-01-01".to_string()))
            .expect("failed to do task");
        tasks
            .do_task("1".to_string(), Some("2024-01-02".to_string()))
            .expect("failed to do task");

        tasks.add("test 2".to_string()).expect("failed to add task");
        tasks
            .do_task("2".to_string(), Some("2023-01-01".to_string()))
            .expect("failed to do task");
        tasks
            .do_task("2".to_string(), Some("2023-01-02".to_string()))
            .expect("failed to do task");
        tasks
            .do_task("2".to_string(), Some("2024-01-01".to_string()))
            .expect("failed to do task");
        tasks
            .do_task("2".to_string(), Some("2024-01-02".to_string()))
            .expect("failed to do task");
    }
}
